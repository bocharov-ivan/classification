#include <iostream>

#include "TPreprocessor.h"

#include "utilities.h"

TPreprocessor::TPreprocessor(void)
{
	Vocabulary = new map<std::string,int>();
	Labels = new map<std::string, int>();
}


TPreprocessor::~TPreprocessor(void)
{
	delete Vocabulary;
	delete Labels;
}

int TPreprocessor::VocabularySize()
{
	return Vocabulary->size();
}

std::vector<TDocument> TPreprocessor::Preprocess(const std::vector<std::string> &collection){
	
	std::vector<TDocument> result(collection.size());
	
	
	for (size_t docNumber = 0; docNumber < collection.size(); ++docNumber) {
		std::cout<<"Doc:"<<docNumber<<"\n";
		TDocument& doc = result[docNumber];

		std::map<int,double> features;
		
		std::vector<std::string> content = string_split(collection[docNumber],'\t');

		std::vector<std::string> labels = string_split(content[0],' ');

		std::vector<std::string> words = string_split(content[1], ' ');
		
		std::vector<int> lbls(labels.size());
		
		for (size_t labelNumber =0;labelNumber<labels.size(); labelNumber++)
		{
			//extract label
			if(Labels->find(labels[labelNumber]) == Labels->end()){
				(*Labels)[labels[labelNumber]]=Labels->size();
			}
			lbls[labelNumber]=((*Labels)[labels[labelNumber]]);
		}
		//extract features
		for (auto word = words.begin(); word<words.end(); word++){
			if (Vocabulary->find(*word) == Vocabulary->end()){
				(*Vocabulary)[*word] = Vocabulary->size();
			}
			features[(*Vocabulary)[*word]]++;
		}
		doc = TDocument(features, lbls);
	}
	std::cout<<"Label count: "<<Labels->size()<<"\n";
	return result;
}
std::vector<TDocument> TPreprocessor::Preprocess(const std::vector<std::string> &collection, int limit){
	
	std::vector<TDocument> result(collection.size());
	
	
	for (size_t docNumber = 0; docNumber < collection.size(); ++docNumber) {
		TDocument& doc = result[docNumber];

		std::map<int,double> features;
		
		std::vector<std::string> content = string_split(collection[docNumber],'\t');

		std::vector<std::string> labels = string_split(content[0],' ');

		std::vector<std::string> words = string_split(content[1], ' ');
		
		std::vector<int> lbls(labels.size());
		
		for (size_t labelNumber =0;labelNumber<labels.size(); labelNumber++)
		{
			//extract label
			if(Labels->find(labels[labelNumber]) == Labels->end()){
				(*Labels)[labels[labelNumber]]=Labels->size();
			}
			lbls[labelNumber]=((*Labels)[labels[labelNumber]]);
		}
		//extract features
		for (size_t wordNumber = 0; wordNumber<words.size() && wordNumber<limit; wordNumber++){
			if (Vocabulary->find(words[wordNumber]) == Vocabulary->end()){
				(*Vocabulary)[words[wordNumber]] = Vocabulary->size();
			}
			features[(*Vocabulary)[words[wordNumber]]]++;
		}
		doc = TDocument(features, lbls);
	}
	std::cout<<"Label count: "<<Labels->size()<<"\n";
	return result;
}

std::vector<TDocument> TPreprocessor::PreprocessWithWeighting(const std::vector<std::string>& Collection, const TWeighter& Weighter){
	
	std::vector<TDocument> result(Collection.size());
	
	for (size_t docNumber = 0; docNumber < Collection.size(); ++docNumber) {
		TDocument& doc = result[docNumber];

		std::map<int,double> features;
		
		std::vector<std::string> content = string_split(Collection[docNumber],'\t');

		std::vector<std::string> labels = string_split(content[0], ' ');
		
		std::vector<std::string> words = string_split(content[1], ' ');
		
		std::vector<int> lbls(labels.size());
		
		for (size_t labelNumber =0;labelNumber<labels.size(); labelNumber++)
		{
			//extract label
			if(Labels->find(labels[labelNumber]) == Labels->end()){
				(*Labels)[labels[labelNumber]]=Labels->size();
			}
			lbls[labelNumber]=((*Labels)[labels[labelNumber]]);
		}
		//extract features
		for (size_t wordNumber = 0; wordNumber<words.size(); wordNumber++){
			if (Vocabulary->find(words[wordNumber]) == Vocabulary->end()){
				(*Vocabulary)[words[wordNumber]] = Vocabulary->size();
			}
			
			features[(*Vocabulary)[words[wordNumber]]]+=Weighter.WeightingFunction(wordNumber);
		
		}
		doc = TDocument(features, lbls);
	}
	//std::cout<<"Label count: "<<Labels->size()<<"\n";
	//for (std::map<std::string, int>::const_iterator it = Labels->begin(); it!=Labels->end(); ++it){
	//	std::cout<<"Label: "<<it->first<<" Number: "<<it->second<<"\n";
	//}

	return result;


}

std::vector<TDocument> TPreprocessor::PreprocessWithSplitting(const std::vector<std::string> &collection){
	
	std::vector<TDocument> result(collection.size());
	
	
	for (size_t docNumber = 0; docNumber < collection.size(); ++docNumber) {
		TDocument& doc = result[docNumber];

		std::map<int,double> features;
		
		std::vector<std::string> content = string_split(collection[docNumber],' ');

		std::vector<std::string> labels, words;

		size_t wordNumber=0;
		
		for ( ; wordNumber<content.size()&&!IsNumber(content[wordNumber]); wordNumber++){
			labels.push_back(content[wordNumber]);
		}

		words.insert(words.end(), content.begin()+wordNumber, content.end());
		
		std::vector<int> lbls(labels.size());
		
		for (size_t labelNumber =0;labelNumber<labels.size(); labelNumber++)
		{
			//extract label
			if(Labels->find(labels[labelNumber]) == Labels->end()){
				(*Labels)[labels[labelNumber]]=Labels->size();
			}
			lbls[labelNumber]=((*Labels)[labels[labelNumber]]);
		}
		//extract features
		for (size_t wordNumber = 0; wordNumber<words.size(); wordNumber++){
			if (Vocabulary->find(words[wordNumber]) == Vocabulary->end()){
				(*Vocabulary)[words[wordNumber]] = Vocabulary->size();
			}
			
			features[(*Vocabulary)[words[wordNumber]]]++;
		
		}
		doc = TDocument(features, lbls);
	}
	//std::cout<<"Label count: "<<Labels->size()<<"\n";
	//for (std::map<std::string, int>::const_iterator it = Labels->begin(); it!=Labels->end(); ++it){
	//	std::cout<<"Label: "<<it->first<<" Number: "<<it->second<<"\n";
	//}

	return result;
}

