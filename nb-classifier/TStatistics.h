#include "TDocument.h"
#include <vector>

#pragma once

using namespace std;

struct ClassificationResult{
	double Precision;
	double Recall;
	double FMeasure;
};

bool CompareCategories(std::pair<int,int>, std::pair<int,int>);

class TStatistics
{
private:
	vector<int> TruePositives;
	vector<int> FalsePositives;
	vector<int> FalseNegatives;

	std::map<int,int> DocumentCounts;
	vector<int> TopCategories;
	void GetTopCategories(int);

	void CheckDocument(const TDocument&);
	int TopCategoriesCount;

	double Alpha;

public:
	TStatistics(void);
	~TStatistics(void);
	
	void CalculateStats(const vector<TDocument>&, const double);
	
	ClassificationResult GetAverageStats();

	double GetPrecision(int) const;
	double GetRecall(int) const;
	double GetFMeasure(int) const;

	double MicroAveragePrecision();
	double MicroAverageRecall();
	double MicroAverageFMeasure();
	
	double MacroAveragePrecision();
	double MacroAverageRecall();
	double MacroAverageFMeasure();
	
	void OutputStats(ostream&);
    
	void SetTopCategoriesCount(int);
	int GetTopCategoriesCount();
	double GetAlpha() {return Alpha;}
	void SetAlpha(double value){ if (value>0) Alpha = value;}
	void OutputForMeasurement(ostream&);
};