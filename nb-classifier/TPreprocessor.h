#pragma once

#include <vector>
#include <string>
#include <map>
#include "TDocument.h"
#include "TWeighter.h"

using namespace std;

class TPreprocessor
{
private:
	std::map<std::string, int>* Vocabulary;
	std::map<std::string, int>* Labels;

public:
	int VocabularySize();
	//preprocessing of the text files
	std::vector<TDocument> Preprocess(const std::vector<std::string>&);
	std::vector<TDocument> Preprocess(const std::vector<std::string>&, int);
	std::vector<TDocument> PreprocessWithWeighting(const std::vector<std::string>&, const TWeighter&);
	std::vector<TDocument> PreprocessWithSplitting(const std::vector<std::string>&);
	


	TPreprocessor(void);
	~TPreprocessor(void);
};

