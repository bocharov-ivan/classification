#pragma once
#include <vector>
#include <utility>
#include <algorithm>


using namespace std;

template <typename T>
class TSplittableDataset
{
private:
	vector<T> Collection;
	vector<vector<T>> StratifiedCollection;
public:
	int Compare(const void*,const void*);
	void Split(int);
	void StratifiedSplit(int);
	pair<vector<T>,vector<T>> Merge(int);
	
	void Reset();
	TSplittableDataset(void);
	TSplittableDataset(const vector<T>&);
	~TSplittableDataset(void);
};

template <typename T>
TSplittableDataset<T>::TSplittableDataset(void)
{
	Collection = vector<T>();
}

template<typename T>
TSplittableDataset<T>::TSplittableDataset(const vector<T>& collection)
{
	Collection = collection;
}

template <typename T>
TSplittableDataset<T>::~TSplittableDataset(void)
{

}

template <typename T>
int TSplittableDataset<T>::Compare(const void* a, const void* b){
	return (T)a->GetGivenLabel()-(T)b->GetGivenLabel();
}

template <typename T>
void TSplittableDataset<T>::StratifiedSplit(int folds){
	
	map<int, vector<T>> splittedByLabels = map<int, vector<T>>();

	StratifiedCollection = vector<vector<T>>(folds);

	for (size_t elemNumber = 0; elemNumber<Collection.size(); elemNumber++){
		for (size_t labelNumber = 0; labelNumber<Collection[elemNumber].GetGivenLabels().size(); labelNumber++)
			splittedByLabels[Collection[elemNumber].GetGivenLabels()[labelNumber]].push_back(Collection[elemNumber]);
	}

	for (std::map<int, std::vector<T>>::const_iterator currentLabel = splittedByLabels.begin(); currentLabel!=splittedByLabels.end(); currentLabel++){
		vector<T> labelContent = currentLabel->second;
		std::random_shuffle(labelContent.begin(), labelContent.end());
		for (size_t docNumber=0;docNumber<labelContent.size();docNumber++){
			StratifiedCollection[docNumber%folds].push_back(labelContent[docNumber]);
		}
	}
	
}



template<typename T>
void TSplittableDataset<T>::Split(int folds){
	
	int elementsInFold = Collection.size()/folds;
	
	for (size_t foldNumber=0; foldNumber<folds-1; foldNumber++){
		std::copy(Collection.begin()+foldNumber*elementsInFold, Collection.begin()+(foldNumber+1)*elementsInFold, StratifiedCollection[foldNumber]);
	}

	std::copy(Collection.begin()+elementsInFold*(folds-1), Collection.end(), StratifiedCollection[folds-1]);
}

template <typename T>
void TSplittableDataset<T>::Reset(){
	StratifiedCollection.clear();
}

template <typename T>
pair<vector<T>,vector<T>> TSplittableDataset<T>::Merge(int fold){
	pair<vector<T>,vector<T>> result;

	result.second = StratifiedCollection[fold];

	for (size_t foldNumber=0; foldNumber<StratifiedCollection.size(); foldNumber++){
		
		if (foldNumber!=fold) result.first.insert(result.first.end(),StratifiedCollection[foldNumber].begin(), StratifiedCollection[foldNumber].end());
	}

	return result;
}