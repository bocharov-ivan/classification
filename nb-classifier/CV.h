#pragma once
#include "TSplittableDataset.h"
#include "TStatistics.h"
#include <vector>
#include <iostream>

//Template-based cross-validation
template< typename ClassifierType, typename ObjectType>
class CV
{
private:
	ClassifierType Classifier;
	TSplittableDataset<ObjectType> Collection;
public:
	CV(void);
	~CV(void);

	CV(ClassifierType&, TSplittableDataset<ObjectType>&);
	ClassificationResult RunCV(int);
};


template< typename ClassifierType, typename ClassifiedObjects>
CV<ClassifierType, ClassifiedObjects>::CV(void)
{
}

template< typename ClassifierType, typename ClassifiedObjects>
CV<ClassifierType, ClassifiedObjects>::CV(ClassifierType& classifier, TSplittableDataset<ClassifiedObjects>& collection):Classifier(classifier), Collection(collection)
{
	Collection.Reset();
}

template< typename ClassifierType, typename ClassifiedObjects>
CV<ClassifierType, ClassifiedObjects>::~CV(void)
{

}

template< typename ClassifierType, typename ClassifiedObjects>
ClassificationResult CV<ClassifierType, ClassifiedObjects>::RunCV(int folds){
	
	vector<ClassificationResult> results(folds);

	Collection.Reset();
	Collection.StratifiedSplit(folds);

	for (int foldNumber=0; foldNumber<folds;foldNumber++){
		try{
		pair<vector<ClassifiedObjects>, vector<ClassifiedObjects>> elementCollections = Collection.Merge(foldNumber);
		Classifier.Reset();
		Classifier.Learn(elementCollections.first);
		Classifier.Classify(elementCollections.second);
		
		TStatistics statistics = TStatistics();
		statistics.CalculateStats(elementCollections.second, 0.5);
		results[foldNumber] = statistics.GetAverageStats();
		std::cout<<"Fold "<<foldNumber+1<<" done!\n";
		}
		catch(...){std::cout<<"Something went wrong...\n";}
	}

	ClassificationResult result;
	result.Precision = 0;
	result.Recall = 0;
	result.FMeasure = 0;

	for (size_t foldNumber=0; foldNumber<results.size(); foldNumber++){
		result.Precision+=results[foldNumber].Precision;
		result.Recall+=results[foldNumber].Recall;
		result.FMeasure+=results[foldNumber].FMeasure;
	}
	
	result.Precision/=folds;
	result.FMeasure/=folds;
	result.Recall/=folds;

	return result;
}
