#include <iostream>

#include <fstream>
#include <sstream>
#include <string>
#include <ctime>

#include <time.h>

#include "CV.h"
#include "TNaiveClassifier.h"
#include "TPreprocessor.h"
#include "TDocument.h"
#include "TStatistics.h"
#include "utilities.h"
#include "TSplittableDataset.h"

#include <boost/program_options.hpp>

using namespace boost;
namespace options = boost::program_options;

using namespace std;

void SetupWeighter(TSigmoidWeighter& Weighter, const options::variables_map& ClassifierOptions){
	if (ClassifierOptions.count("k1"))
		Weighter.SetK1(ClassifierOptions["k1"].as<double>());
	else Weighter.SetK1(1);
	
	if (ClassifierOptions.count("k2"))
		Weighter.SetK1(ClassifierOptions["k2"].as<double>());
	else Weighter.SetK2(1);
	
}




void PerformCV(options::variables_map& ClassifierOptions){
	std::cout<<"Naive Bayes test:\n";
		
	try{
			vector<TDocument> CVCollection = vector<TDocument>();

			TNaiveClassifier naiveClassifier = TNaiveClassifier();
		
			naiveClassifier.SetAlpha(ClassifierOptions["alpha"].as<double>());

			TPreprocessor* prep = new TPreprocessor(); 
			std::vector<std::string>* current_set = new std::vector<std::string>();
			vector<TDocument>* train_collection = new vector<TDocument>();
        
			std::fstream file_stream(ClassifierOptions["filename"].as<std::string>());
        
			std::string current_line;

			while (std::getline(file_stream, current_line)){
					current_set->push_back(current_line);
			}
        
			TSigmoidWeighter weighter;
			SetupWeighter(weighter, ClassifierOptions);
		
			std::cout<<"Preprocessing first file...\n";
			CVCollection = prep->PreprocessWithWeighting(*current_set,weighter);
			
		
			std::cout<<"Done!\n";


			//std::cout<<"Reading the file...\n";
			clock_t begin = clock();
			//CVCollection = read_from_file(ClassifierOptions["filename"].as<std::string>());
			//std::cout<<"Time elapsed: "<<double(clock() - begin) / CLOCKS_PER_SEC<<"s.\n\n";

			std::cout<<"Splitting the collection...\n";
			TSplittableDataset<TDocument> splitCollection = TSplittableDataset<TDocument>(CVCollection);

			CV<TNaiveClassifier, TDocument> CVPerformer = CV<TNaiveClassifier,TDocument>(naiveClassifier, TSplittableDataset<TDocument>(CVCollection));

			std::cout<<"Performing Cross-Validation...\n";
			begin = clock();
	
			ClassificationResult stats = CVPerformer.RunCV(ClassifierOptions["folds"].as<int>());
	
			std::cout<<"CV time: "<<double(clock() - begin) / CLOCKS_PER_SEC<<"s.\n\n";
	
			std::cout<<"Calculatin stats...\n\n";
			begin = clock();
	
			ofstream resultStream;
	
			if (ClassifierOptions.count("result")){
				resultStream = ofstream();
				resultStream.open(ClassifierOptions["result"].as<std::string>());
			}
	
			std::ostream& outputStream = (ClassifierOptions.count("result"))?resultStream:std::cout;
	
			if (ClassifierOptions.count("full-stat")){

			outputStream<<"=============================================\n";
			outputStream<<"Average stats:\n";
			outputStream<<"==============================================\n";
			outputStream<<"Average precision: "<<stats.Precision<<"\n";
			outputStream<<"Average recall: "<<stats.Recall<<"\n";
			outputStream<<"Average fmeasure: "<<stats.FMeasure<<"\n";
			outputStream<<"==============================================\n";
			}

			else {
				outputStream<<"precision:"<<stats.Precision<<"\n";
				outputStream<<"recall:"<<stats.Recall<<"\n";
				outputStream<<"fmeasure:"<<stats.FMeasure<<"\n";
			}
			std::cout<<"Done!\n";
			}
			catch (std::exception& exc){
				std::cout<<"Error: "<<exc.what()<<"\n";
			}
			std::cout<<"Press the button to exit the application.\n";
			char test;
			cin>>test;
}

void OutputStats(ostream& str, TStatistics& stat){
	
	ClassificationResult result = stat.GetAverageStats();

	str<<"=============================================\n";
	str<<"Average stats:\n";
	str<<"==============================================\n";
	str<<"Average precision: "<<result.Precision<<"\n";
	str<<"Average recall: "<<result.Recall<<"\n";
	str<<"Average fmeasure: "<<result.FMeasure<<"\n";
	str<<"Macroaverage precision: "<<stat.MacroAveragePrecision()<<"\n";
	str<<"Macroaverage recall: "<<stat.MacroAverageRecall()<<"\n";
	str<<"Macroaverage fmeasure: "<<stat.MacroAverageFMeasure()<<"\n";
	str<<"==============================================\n";

}


void Classify(const options::variables_map& ClassifierOptions){
	
	TNaiveClassifier* naiveClassifier = new TNaiveClassifier();
	TPreprocessor* prep = new TPreprocessor(); 

    std::vector<std::string>* current_set = new std::vector<std::string>();


	vector<TDocument>* trainCollection = new vector<TDocument>();
	vector<TDocument>* testCollection = new vector<TDocument>();
        
	try{
	std::fstream train_stream(ClassifierOptions["train"].as<std::string>());
        
    std::string current_line;

    while (std::getline(train_stream, current_line)){
           current_set->push_back(current_line);
    }
        
	TSigmoidWeighter weighter;
	SetupWeighter(weighter, ClassifierOptions);

	*trainCollection = prep->PreprocessWithWeighting(*current_set,weighter);
	naiveClassifier->SetAlpha(ClassifierOptions["alpha"].as<double>());

	std::cout<<"\nLearning the classifier...\n";
	time_t begin = clock();
	naiveClassifier->Learn(*trainCollection);
	std::cout<<"Learn time:"<<double(clock() - begin) / CLOCKS_PER_SEC<<" s.\n\n";
	
	current_set->clear();

	std::fstream test_stream(ClassifierOptions["test"].as<std::string>());

	while (std::getline(test_stream, current_line)){
           current_set->push_back(current_line);
    }

	*testCollection=prep->PreprocessWithWeighting(*current_set, weighter);

	std::cout<<"Classifying...\n";
	begin = clock();
	naiveClassifier->Classify(*testCollection);
	std::cout<<"Classification time: "<<double(clock() - begin) / CLOCKS_PER_SEC<<"s.\n\n";

	TStatistics* stat = new	TStatistics();
	
	std::cout<<"Calculatin stats...\n\n";
	stat->SetTopCategoriesCount(ClassifierOptions["cat"].as<int>());

	stat->CalculateStats(*testCollection,0.5);
	
	ofstream resultStream;
	
	if (ClassifierOptions.count("result")){
		resultStream = ofstream();	
		resultStream.open(ClassifierOptions["result"].as<std::string>());
	}

	std::ostream& outputStream = (ClassifierOptions.count("result"))?resultStream:std::cout;
	if (ClassifierOptions.count("full-stat"))
	stat->OutputStats(outputStream);
	else
		stat->OutputForMeasurement(outputStream);
	}
	catch (std::exception& exc){
		std::cout<<"Error: "<<exc.what()<<"\n";
	}
	std::cout<<"Press the button to exit the application.\n";
	char test;
	cin>>test;
}
