#include <fstream>
#include <ostream>
#include <sstream>
#include <iostream>
#include <locale>
#include "utilities.h"

using namespace std;

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> string_split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

void write_to_file(const std::string filename, vector<TDocument> collection){
	
	ofstream myFile;
	myFile.open(filename);
	
	ostringstream document_content;
	
	for (auto currentDocument = collection.begin(); currentDocument!=collection.end(); currentDocument++){
		
		document_content.str("");
		document_content.clear();

		vector<int>& labels = currentDocument->GetGivenLabels();

		for (size_t i = 0; i<labels.size()-1; i++){
			document_content<<labels[i]<<" ";
		}
		document_content<<labels[labels.size()-1]<<"\t";

		map<int,double> features = currentDocument->GetFeatures();
		for (auto feature = features.begin(); feature!=features.end(); feature++){
			document_content<<feature->first<<' '<<feature->second<<' ';
		}
		myFile<<document_content.str().substr(0, document_content.str().size()-1)<<"\n";
	}
	
}

std::vector<TDocument> read_from_file(const std::string filename){
	std::vector<TDocument> result;

	ifstream myFile;
	myFile.open(filename);
	
	std::string document_content = "";
	
	
	map<int,double> features;
	int first, label = 0;
	double second=0;
	while (std::getline(myFile, document_content)){
		istringstream doc_stream(document_content);
		
		doc_stream>>label;
		
		while(doc_stream>>first>>second){
			features[first] = second;
		}
		
		result.push_back(TDocument(features,label));
		features.clear();
	}
	return result;
}

bool IsNumber(const std::string& inputString){
	return inputString.find_first_not_of("0123456789") == std::string::npos;
}