#include "TDocument.h"
#include "utilities.h"

TDocument::TDocument(void): GivenLabels(), Predictions(), Features()
{
	
}

TDocument::TDocument(const map<int,double>& features, int label): GivenLabels(), Predictions()
{
	Features=features;
	GivenLabels.push_back(label);
}

TDocument::TDocument(const map<int,double>& features, const std::vector<int>& labels): GivenLabels(), Predictions()
{
	Features=features;
	GivenLabels = labels;
}


TDocument::~TDocument(void)
{
	
}

const map<int,double>& TDocument::GetFeatures() const{
	return Features;
}
vector<int> TDocument::GetGivenLabels() const{
	return GivenLabels;
}

vector<std::pair<int,double>> TDocument::GetPredictions() const{
	return Predictions;
}

double TDocument::GetFeature(int key){
		if (this->Features.find(key)!=this->Features.end()){
			return this->Features[key];
		}
		else return -1;
}

void TDocument::SetPredictions(const vector<std::pair<int,double>>& value){
	Predictions = value;
}
bool TDocument::CorrectlyClassified() const{
	return true;
}

int TDocument::GetLabelCount() const{
	return Predictions.size();
}

