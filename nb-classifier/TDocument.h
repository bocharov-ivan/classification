#pragma once
#include <map>
#include <vector>
#include <string>
using namespace std;

class TDocument
{
private:
	std::map<int,double> Features;
	vector<int> GivenLabels;
	vector<std::pair<int,double>> Predictions;

public:
	bool CorrectlyClassified() const;	
	double GetFeature(int);
	vector<int> GetGivenLabels() const;
	vector<std::pair<int,double>> GetPredictions() const;
	const std::map<int,double>& GetFeatures() const;
	void SetPredictions(const vector<std::pair<int,double>>&);
	
	int GetLabelCount() const;

	TDocument(void);
	TDocument(const std::map<int,double>&, int);
	TDocument(const std::map<int,double>&, const std::vector<int>&);
	~TDocument(void);
};

