#pragma once
class TWeighter
{
public:
	TWeighter(void);
	~TWeighter(void);
	virtual double WeightingFunction(int) const = 0;
};

class TSigmoidWeighter:public TWeighter{
private:
	double K1, K2;

public:
	void SetK1(double value){K1 = value;}
	void SetK2(double value){K2 = value;}

	double GetK1(){return K1;}
	double GetK2(){return K2;}
	
	virtual double WeightingFunction(int) const;
};