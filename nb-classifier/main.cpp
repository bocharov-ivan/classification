#include "main.h"

#include <boost/program_options.hpp>

using namespace boost;
namespace options = boost::program_options;

void AddOptions(options::options_description& Options){

	Options.add_options()
		("mode,m", options::value<std::string>(), "Application mode")
		("function",options::value<std::string>(), "Weighting function")
		("k1", options::value<double>(), "Sigmoid parameter")
		("k2", options::value<double>(), "Sigmoid parameter")
		("folds", options::value<int>()->default_value(10), "Validation folds")
		("train", options::value<std::string>(), "Train filepath")
		("test", options::value<std::string>(), "Test filepath")
		("filename,f", options::value<std::string>(), "Collection filepath")
		("train-result", options::value<std::string>(), "Train result filename")
		("test-result", options::value<std::string>(), "Test result filename")
		("result", options::value<std::string>(), "Result filename")
		("cat", options::value<int>()->default_value(5), "Top categories count")
		("alpha,a", options::value<double>()->default_value(1.0), "Regularization parameter")
		("stat", options::value<std::string>(), "Filename with statistics")
		("full-stat", "Full information output");
		;
}

int main(int argc, char* argv[]){
	std::srand(time(NULL));
	try{
	options::options_description classifierOptions("Allowed parameters");
	AddOptions(classifierOptions);
	options::variables_map vm;
	options::store(options::parse_command_line(argc,argv,classifierOptions),vm);
	
	if (vm.count("mode")){
		std::string mode = vm["mode"].as<std::string>();
		if (mode == "classify"){
			Classify(vm);
		}

		else if (mode == "cv"){
				PerformCV(vm);
		}
	}
	else std::cout<<"Provide application mode!\n";}
	catch (std::exception& exc){
		std::cout<<"Error: "<<exc.what()<<"\n";
		char c;
		cin>>c;
	}
	return 0;
}