#include "TStatistics.h"
#include <algorithm>


TStatistics::TStatistics(void): TopCategoriesCount(5), Alpha(0.5)
{
	
}


TStatistics::~TStatistics(void)
{
	
}


#include <iostream>
#include <iomanip>
void TStatistics::CalculateStats(const vector<TDocument>& collection, const double b=0.5){
	
	TruePositives = vector<int>(collection[0].GetLabelCount());
	FalsePositives = vector<int>(collection[0].GetLabelCount());
	FalseNegatives = vector<int>(collection[0].GetLabelCount());
	DocumentCounts = map<int,int>();

	for (size_t docNumber=0; docNumber<collection.size(); ++docNumber){
		
		CheckDocument(collection[docNumber]);
	}
	
	GetTopCategories(TopCategoriesCount);
}

double TStatistics::MicroAveragePrecision(){
	
	double result = 0;
	int denominator = 0;
	for (size_t labelNumber=0; labelNumber<TopCategories.size(); labelNumber++){
		result+=TruePositives[TopCategories[labelNumber]];
		denominator+=TruePositives[TopCategories[labelNumber]];
		denominator+=FalsePositives[TopCategories[labelNumber]];
	}
	return result/denominator;
}
double TStatistics::MicroAverageRecall(){
	double result = 0;
	int denominator = 0;
	for (size_t labelNumber=0; labelNumber<TopCategories.size(); labelNumber++){
		result+=TruePositives[TopCategories[labelNumber]];
		denominator+=TruePositives[TopCategories[labelNumber]];
		denominator+=FalseNegatives[TopCategories[labelNumber]];
	}
	return result/denominator;
}
double TStatistics::MicroAverageFMeasure(){
	double p = MicroAveragePrecision();
	double r = MicroAverageRecall();
	return (2*p*r)/(p+r);
}

ClassificationResult TStatistics::GetAverageStats(){
	ClassificationResult result;
	result.Precision = MicroAveragePrecision();
	result.Recall = MicroAverageRecall();
	result.FMeasure = MicroAverageFMeasure();
	
	return result;
}

void TStatistics::CheckDocument(const TDocument& document){
	size_t predictionNumber=0;
	
	bool firstIncorrectFound = false;

	vector<int> givenLabels = document.GetGivenLabels();
	vector<std::pair<int,double>> predictions = document.GetPredictions();
	
	for (size_t labelNumber=0;labelNumber<givenLabels.size();++labelNumber){
		DocumentCounts[givenLabels[labelNumber]]++;
	}
	
	for (predictionNumber=0; predictionNumber<givenLabels.size(); predictionNumber++){
		if (std::find(givenLabels.begin(), givenLabels.end(), predictions[predictionNumber].first)!=givenLabels.end()){
			if (!firstIncorrectFound){
				TruePositives[predictions[predictionNumber].first]++;
			}
			else{
				FalseNegatives[predictions[predictionNumber].first]++;
			}
		}
		else{
			firstIncorrectFound=true;
			FalsePositives[predictions[predictionNumber].first]++;
		}
	}

	for (predictionNumber; predictionNumber<predictions.size(); ++predictionNumber){
		if (std::find(givenLabels.begin(), givenLabels.end(), predictions[predictionNumber].first)!=givenLabels.end()){
			FalseNegatives[predictions[predictionNumber].first]++;
		}
	}
}

bool CompareCategories(const std::pair<int,int> left, const std::pair<int,int> right){
	return left.second>right.second;
}

void TStatistics::GetTopCategories(int count){
	TopCategories = vector<int>(count);
	std::vector<std::pair<int,int>> categories;

	for(std::map<int,int>::const_iterator category = DocumentCounts.begin(); category!=DocumentCounts.end(); ++category){
		std::pair<int, int> stuff;
		stuff.first = category->first;
		stuff.second = category->second;
		categories.push_back(stuff);
	}
	
	std::sort(categories.begin(), categories.end(), CompareCategories);
	
	for (size_t labelNumber=0;labelNumber<count;labelNumber++){
		TopCategories[labelNumber] = categories[labelNumber].first;
	}
	
}
double TStatistics::GetPrecision(int label) const{
	return (double)TruePositives[label]/(FalsePositives[label]+TruePositives[label]);
}

double TStatistics::GetRecall(int label) const{
	return (double)TruePositives[label]/(FalseNegatives[label]+TruePositives[label]);
}

double TStatistics:: GetFMeasure(int label) const{
	return 1/(Alpha/GetPrecision(label) + (1-Alpha)/GetRecall(label));
}

double TStatistics::MacroAverageFMeasure(){
	return (2*MacroAveragePrecision()*MacroAverageRecall()/(MacroAveragePrecision()+MacroAverageRecall()));
}
double TStatistics::MacroAveragePrecision(){
	double result = 0;
	for (size_t labelNumber=0;labelNumber<TruePositives.size();labelNumber++){
		result+=GetPrecision(TopCategories[labelNumber]);
	}	
	result/=TruePositives.size();
	return result;
}
double TStatistics::MacroAverageRecall(){
	double result = 0;
	for (size_t labelNumber=0;labelNumber<TruePositives.size();labelNumber++){
		result+=GetRecall(TopCategories[labelNumber]);
	}	
	result/=TruePositives.size();
	return result;
}
void TStatistics::OutputStats(ostream& OutputStream){
	
	OutputStream<<"=============================================\n";
	OutputStream<<"L   P      R      F1\n";
	for (size_t labelNumber = 0; labelNumber<TopCategories.size();++labelNumber){
		OutputStream<<TopCategories[labelNumber]<<" "<<std::fixed<<std::setfill('0')<<std::setprecision(4)<<GetPrecision(TopCategories[labelNumber])<<" "<<setprecision(4)<<GetRecall(TopCategories[labelNumber])<<" "<<setprecision(4)<<GetFMeasure(TopCategories[labelNumber])<<"\n";
	}
	
	OutputStream<<"=============================================\n\n\n";
	
	ClassificationResult result = GetAverageStats();

	OutputStream<<"=============================================\n";
	OutputStream<<"Average stats:\n";
	OutputStream<<"==============================================\n";
	OutputStream<<"Average precision: "<<result.Precision<<"\n";
	OutputStream<<"Average recall: "<<result.Recall<<"\n";
	OutputStream<<"Average fmeasure: "<<result.FMeasure<<"\n";
	OutputStream<<"==============================================\n\n\n\n";	
}

void TStatistics::OutputForMeasurement(ostream& OutputStream){	
	
	ClassificationResult result = GetAverageStats();

	OutputStream<<"precision:"<<result.Precision<<"\n";
	OutputStream<<"recall:"<<result.Recall<<"\n";
	OutputStream<<"fmeasure:"<<result.FMeasure<<"\n";
}

int TStatistics::GetTopCategoriesCount(){
	return TopCategoriesCount;
}

void TStatistics::SetTopCategoriesCount(int value){
	if (value>0){
		TopCategoriesCount = value;
	}
}