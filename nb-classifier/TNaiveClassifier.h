#pragma once

#include <map>
#include <vector>
#include "TDocument.h"
#include "TWeighter.h"

using namespace std;

class TNaiveClassifier{

	private:
		//Total document count
		int TotalDocumentCount;
		//Overall vocabulary size
		double VocabularySize;
		//Word weights for every label [label:[word:weight]]
		std::map<int,std::map<int, double>> FeatureWeights;
		//Aggregated feature weights for each label [label:sum of feature weights]
		std::map<int, double> AggregatedFeatureWeights;
		//Counts of different documents for each label
		std::map<int,int> LabelDocumentCounts;
		
		void GetProbabilities(const TDocument&, vector<std::pair<int,double>>&) const;
		double GetProbability(const TDocument&, const int) const;
		
		double GetDocumentProbability(const TDocument&, int)const;
		double GetLabelProbability(const int) const;
		double GetFeatureProbability(const int, const double, const int) const;

		double FeatureCount(const int, const int) const;

		double Alpha;

	public:
		void Reset();
		void Learn(const vector<TDocument>&);
		void Learn(const vector<TDocument>&, const TWeighter&);

		void Classify(vector<TDocument>&) const;
		void Classify(vector<TDocument>&, const TWeighter&) const;

		void Classify(TDocument&) const;
		void CLassify(TDocument&, const TWeighter&) const;

		double GetAlpha(){return Alpha;}
		void SetAlpha(double value){if ((value>0)&&(value<=1)) Alpha = value;}

		TNaiveClassifier();
		~TNaiveClassifier();
};

bool Compare (std::pair<int,double>,std::pair<int,double>);