#pragma once

#include <vector>

using namespace std;
template <class T>
class TSplittingCollection
{
private:
	vector<T> Collection;
public:
	TSplittingCollection(void);
	~TSplittingCollection(void);
	vector<vector<T>> SplitCollection();
};

