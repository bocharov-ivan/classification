#include "TNaiveClassifier.h"
#include <algorithm>

TNaiveClassifier::TNaiveClassifier(): FeatureWeights(), LabelDocumentCounts(), AggregatedFeatureWeights(), Alpha(1), TotalDocumentCount(0),VocabularySize(0){
	
}

TNaiveClassifier::~TNaiveClassifier(){
	
}

void TNaiveClassifier::Classify(TDocument& document) const{
		
	vector<std::pair<int,double>> predictions;
	GetProbabilities(document, predictions);
	std::sort(predictions.begin(), predictions.end(), Compare);
	document.SetPredictions(predictions);
}

void TNaiveClassifier::Classify(vector<TDocument>& testSet) const{
	for(size_t i = 0;i<testSet.size();i++){
		Classify(testSet[i]);
	}
}

void TNaiveClassifier::GetProbabilities(const TDocument& document, vector<std::pair<int,double>>& probabilities) const{
	int label_count = FeatureWeights.size();
	probabilities = vector<std::pair<int,double>>(label_count);
	for (int label=0; label<label_count; label++){
		probabilities[label]=std::pair<int,double>(label,GetProbability(document, label));
	}
}

double TNaiveClassifier::GetProbability(const TDocument& doc, int label) const{
	return GetLabelProbability(label)+GetDocumentProbability(doc, label);
}

double TNaiveClassifier::GetLabelProbability(int label)const{
	int lcount = 0;
	try{
		lcount = LabelDocumentCounts.at(label);
	}
	//No documents in learning set
	catch(...){	}
	return log((double)(lcount)/TotalDocumentCount);
}

double TNaiveClassifier::GetDocumentProbability(const TDocument& doc, int label) const {
	double probability = 0;
	map<int, double> features = doc.GetFeatures();

	for (map<int,double>::const_iterator feature = features.begin(); feature != features.end(); feature++){
		probability += log(GetFeatureProbability(feature->first, feature->second,label));
	}
	
	return probability;
}

double TNaiveClassifier::GetFeatureProbability(const int key, const double value, const int label) const{
	try{
		return (FeatureCount(key, label)+Alpha)/(AggregatedFeatureWeights.at(label)+VocabularySize*Alpha);
	}
	//No documents in learning set
	catch(...){
		return 0;
	}
}

double TNaiveClassifier::FeatureCount(const int feature, const int label) const{

	try{
		return FeatureWeights.at(label).at(feature);
	}
	catch(...){
		return 0;
	}
}

void TNaiveClassifier::Learn(const vector<TDocument>& trainSet){
	
	for(size_t i = 0; i<trainSet.size(); i++){
		for (size_t j=0;j<trainSet[i].GetGivenLabels().size(); j++){
			int label = trainSet[i].GetGivenLabels()[j];
			this->LabelDocumentCounts[label]+=1;
			TotalDocumentCount++;
			map<int,double> features = trainSet[i].GetFeatures();

			for (map<int,double>::const_iterator feature = features.begin(); feature != features.end(); feature++){
			
				int key = feature->first;
				double value = feature->second;
			
				this->FeatureWeights[label][key]+=value;
				
				this->AggregatedFeatureWeights[label]+=value;
				this->VocabularySize+=value;
			}
		}
	}
}

void TNaiveClassifier::Reset(){
	FeatureWeights.clear();
	LabelDocumentCounts.clear();
	AggregatedFeatureWeights.clear();
	VocabularySize = 0;
}

bool Compare (std::pair<int,double> left, std::pair<int,double> right){
	return left.second>right.second;
}
