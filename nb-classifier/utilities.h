#pragma once
#include <vector>
#include <string>
#include <sstream>

#include "TDocument.h"

using namespace std;

std::vector<std::string>& my_split(const std::string, char, std::vector<std::string>&);

std::vector<std::string> string_split(const std::string &, char);

void write_to_file(const std::string, const vector<TDocument>);

std::vector<TDocument> read_from_file(const std::string);

bool IsNumber(const std::string&);


//Template- and STL-based collection of objects
