#include "TWeighter.h"
#include <math.h>

TWeighter::TWeighter(void)
{
}


TWeighter::~TWeighter(void)
{
}

double TSigmoidWeighter::WeightingFunction(int Position) const{
	double numerator = 1 + exp(-K1*K2);
	double denominator = 1 + exp(-K1*(K2-Position));
	return numerator/denominator;
}